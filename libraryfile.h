#pragma once
#include <stdio.h>
#include <stdlib.h>

#define max_callbacks 6
typedef void (*function_pointer)(void);

function_pointer event_one_callback[max_callbacks];
function_pointer event_two_callback[max_callbacks];

int register_callback_for_event_one(function_pointer callback);
int register_callback_for_event_two(function_pointer callback);

void event_one();
void event_two();
#include <stdio.h>
#include <stdlib.h>
#include "libraryfile.h"

void print_satyam()
{
    printf("satyam \n");
}

void print_abhijan()
{
    printf("abjijan \n");
}

void print_bikash()
{
    printf("bikash \n");
}
void print_mars()
{
    printf("mars \n");
}

int main()
{
    register_callback_for_event_one(print_satyam);
    register_callback_for_event_one(print_mars);
    register_callback_for_event_two(print_bikash);
    register_callback_for_event_two(print_abhijan);

    // simulating events triggered
    event_one();
    event_two();
}

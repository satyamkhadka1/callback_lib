#include "libraryfile.h"

int num_registered_callbacks_one = 0; //number of callbacks registered
int num_registered_callbacks_two = 0;

int register_callback_for_event_one(function_pointer callback)
{
    if (num_registered_callbacks_one > max_callbacks)
    {
        printf("Callback one array is full");
        return 0;
    }
    event_one_callback[num_registered_callbacks_one] = callback;
    num_registered_callbacks_one++;
}

int register_callback_for_event_two(function_pointer callback)
{
    if (num_registered_callbacks_two > max_callbacks)
    {
        printf("Callback two array is full");
        return 0;
    }
    event_two_callback[num_registered_callbacks_two] = callback;
    num_registered_callbacks_two++;
}

void event_one()
{
    if (num_registered_callbacks_one)
    {
        for (int i = 0; i < num_registered_callbacks_one; i++)
        {
            event_one_callback[i]();
        }
    }
    return;
}

void event_two()
{
    if (num_registered_callbacks_two)
    {
        for (int i = 0; i < num_registered_callbacks_two; i++)
        {
            event_two_callback[i]();
        }
    }
    return;
}
